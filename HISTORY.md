# History of versions #

* Version 16 (2020-10-12)
      * Removed several warning messages in the logs
* Version 15 (2020-10-12)
      * Now supports DnD and right-click menu
* Version 14
      * Ported to Gnome Shell 3.38
* Version 13 (2020-04-18)
      * Removed calls to deprecated methods
      * Restored property in gschema to avoid failure on update
* Version 12 (2020-04-15)
      * Support for Gnome Shell 3.36
* Version 11 (2020-04-10)
      * Doesn't fail when there are Wine applications
* Version 10 (2019-04-08)
      * Fixes bug that set gnome shell with mixed languages
* Version 9 (2018-09-18)
      * Do the changes only after the startup process
      * Removed deprecated methods
* Version 8 (2018-09-14)
      * Fixed the name for the buttons, to avoid a clash
* Version 7 (2016-12-29)
      * Allows to choose whether to show or not the "Favorites" and "Frequent" buttons
      * Added translations for the settings window
* Version 6 (2016-12-28)
      * Litle stupid change
* Version 5 (2016-12-28)
      * Ensures that no foraneous elements remain in the system
* Version 4 (2016-12-28)
      * Added "Frequent apps" button
      * Better memory management
* Version 3 (2016-12-28)
      * Added "Favorites apps" button
* Version 2 (2016-12-07)
      * More elegant way for inserting the elements in the activities window
      * Now, when uninstalling the extension, removes all actors (forgot to remove appsLaunchContainer)
* Version 1 (2016-12-07)
      * First public version
