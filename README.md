# ActivityAppLauncher #

A gnome shell extension that adds a category-based application launcher to the Activities view.

By default, the Activities view works exactly as usual (it shows the windows list and allows to find apps by typing), but when the user clicks on a category, a list with the applications in that category is shown.

Note: the dash has been moved to the bottom and converted in a dock thanks to ["Dash to dock"](https://extensions.gnome.org/extension/307/dash-to-dock/) extension, which is NOT part of activityAppLauncher.

IMPORTANT: requires "libgnome-menu-3-dev". It must be installed BEFORE trying to install this extension.

![ScreenShot](activitiesAppLauncher.jpg)

## Quick install

To just install the extension in your local folder, just run the `local_install.sh` script. It requires
`meson`, `ninja` and `xgettext`, so be sure that you have them installed in your system.

## Build with Meson

The project uses a build system called [Meson](https://mesonbuild.com/). You can install
in most Linux distributions as "meson". You also need "ninja" and xgettext.

It's possible to read more information in the Meson docs to tweak the configuration if needed.

For a regular use and local development these are the steps to build the
project and install it:

```bash
meson --prefix=$HOME/.local/ --localedir=share/gnome-shell/extensions/activityAppLauncher@rastersoft.com/locale .build
ninja -C .build install
```

It is strongly recommended to delete the destination folder
($HOME/.local/share/gnome-shell/extensions/activityAppLauncher@rastersoft.com) before doing this, to ensure that no old
data is kept.

## Export extension ZIP file for extensions.gnome.org

To create a ZIP file with the extension, just run:

```bash
./export-zip.sh
```

This will create the file `activityAppLauncher@rastersoft.com.zip` with the extension, following the rules for publishing at extensions.gnome.org.

## Contacting the author

Sergio Costas Rodríguez  
rastersoft@gmail.com  
http://www.rastersoft.com  
https://gitlab.com/rastersoft/activityAppLauncher
